/*
 * Copyright (C) 2022 The Nameless-AOSP Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.internal.util.custom;

import android.content.Context;
import android.content.om.IOverlayManager;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.os.UserHandle;
import android.provider.Settings;

public class QSLayoutUtils {

    public static boolean getQSTileLabelHide(Context context) {
        return Settings.System.getIntForUser(context.getContentResolver(),
                Settings.System.QS_TILE_LABEL_HIDE,
                0, UserHandle.USER_CURRENT) == 1;
    }

    public static boolean getQSTileVerticalLayout(Context context) {
        return Settings.System.getIntForUser(context.getContentResolver(),
                Settings.System.QS_TILE_VERTICAL_LAYOUT,
                0, UserHandle.USER_CURRENT) == 1;
    }

    public static boolean updateLayout(Context context) {
        final IOverlayManager overlayManager = IOverlayManager.Stub.asInterface(ServiceManager.getService(
                Context.OVERLAY_SERVICE));
        final int layout_qs = Settings.System.getIntForUser(context.getContentResolver(),
                Settings.System.QS_LAYOUT,
                42, UserHandle.USER_CURRENT);
        final int layout_qqs = Settings.System.getIntForUser(context.getContentResolver(),
                Settings.System.QQS_LAYOUT,
                22, UserHandle.USER_CURRENT);
        final int row_qs = layout_qs / 10;
        final int col_qs = layout_qs % 10;
        final int row_qqs = layout_qqs / 10;
        for (int i = 0; i < 2; ++i) {
            String pkgName;
            if (i == 0) {
                pkgName = String.format("com.nameless.qs.portrait.layout_%sx%s", Integer.toString(row_qs), Integer.toString(col_qs));
            } else {
                pkgName = String.format("com.nameless.qqs.portrait.layout_%sx%s", Integer.toString(row_qqs), Integer.toString(col_qs));
            }
            try {
                overlayManager.setEnabledExclusiveInCategory(pkgName, UserHandle.USER_CURRENT);
            } catch (RemoteException re) {
                return false;
            }
        }
        return true;
    }
}
