/*
 * Copyright (C) 2022 The Nameless-AOSP Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.internal.util.custom;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.SystemProperties;
import android.util.Log;

public class LauncherUtils {

    private static final String TAG = "LauncherUtils";

    private static final String RECENT_COMPONENT_NAME = "/com.android.quickstep.RecentsActivity";

    private static final String LAWNCHAIR_OVERLAY_PKG_NAME = "com.android.launcher.recentsComponent.overlay";
    private static final String LAWNCHAIR_PKG_NAME = "app.lawnchair";

    private static final String NAMELESS_LAUNCHER_PKG_NAME = "com.android.launcher3";
    private static final String NAMELESS_CLASS_NAME = "org.nameless.launcher.NamelessLauncher";

    private static final String PIXEL_LAUNCHER_PKG_NAME = "com.google.android.apps.nexuslauncher";

    private static final String KEY_SETTINGS = "persist.sys.custom.launcher";
    private static final String KEY_CACHED = "sys.custom.launcher_cached";

    private static final int UNAVAILABLE = 0;
    private static final int PIXEL = 1;
    private static final int NAMELESS = 2;
    private static final int LAWNCHAIR = 4;

    private static final int LAUNCHER_PIXEL = 0;
    private static final int LAUNCHER_NAMELESS = 1;
    private static final int LAUNCHER_LAWNCHAIR = 2;
    private static final int LAUNCHER_UNAVAILABLE = 3;

    private static boolean checkNamelessLauncher(Context context, boolean checkClass) {
        try {
            PackageInfo pi = context.getPackageManager().getPackageInfo(NAMELESS_LAUNCHER_PKG_NAME, 0);
            if (checkClass && pi.applicationInfo.enabled) {
                Intent intent = new Intent();
                ComponentName cn = new ComponentName(NAMELESS_LAUNCHER_PKG_NAME, NAMELESS_CLASS_NAME);
                intent.setComponent(cn);
                if (intent.resolveActivityInfo(context.getPackageManager(),
                        PackageManager.MATCH_DEFAULT_ONLY) == null) {
                    return false;
                }
            }
        } catch (NameNotFoundException e) {
            return false;
        }
        return true;
    }

    public static int getAvailableStatus(Context context, boolean checkClass) {
        int ret = UNAVAILABLE;
        if (CustomUtils.isPackageInstalled(context, PIXEL_LAUNCHER_PKG_NAME)) {
            ret |= PIXEL;
        }
        if (checkNamelessLauncher(context, checkClass)) {
            ret |= NAMELESS;
        }
        if (CustomUtils.isPackageInstalled(context, LAWNCHAIR_OVERLAY_PKG_NAME) &&
                CustomUtils.isPackageInstalled(context, LAWNCHAIR_PKG_NAME)) {
            ret |= LAWNCHAIR;
        }
        return ret;
    }

    public static boolean hasNoLauncher(int status) {
        return status == UNAVAILABLE;
    }

    public static boolean isPixelAvailable(int status) {
        return (status & PIXEL) != 0;
    }

    public static boolean isNamelessAvailable(int status) {
        return (status & NAMELESS) != 0;
    }

    public static boolean isLawnchairAvailable(int status) {
        return (status & LAWNCHAIR) != 0;
    }

    public static int getLauncher() {
        return SystemProperties.getInt(KEY_SETTINGS, LAUNCHER_PIXEL);
    }

    public static void setLauncher(int launcher) {
        SystemProperties.set(KEY_SETTINGS, Integer.toString(launcher));
    }

    private static int ensureCorrectLauncher(int launcher, String componentName) {
        final String launcherPackage = componentName.substring(0, componentName.indexOf("/"));
        if (!launcherPackage.equals(PIXEL_LAUNCHER_PKG_NAME)) {
            // Default component name is changed, possibly launcher modules installed.
            if (launcherPackage.equals(NAMELESS_LAUNCHER_PKG_NAME)) {
                return LAUNCHER_NAMELESS;
            }
            if (launcherPackage.equals(LAWNCHAIR_PKG_NAME)) {
                return LAUNCHER_LAWNCHAIR;
            }
            // Unsupport launcher. Set unavailable and return config value for component name.
            return LAUNCHER_UNAVAILABLE;
        }
        return launcher;
    }

    public static int getRealLauncher(Context context) {
        final String resComponentName = context.getString(
                com.android.internal.R.string.config_recentsComponentName);
        int launcher = getLauncher();
        if (launcher != LAUNCHER_UNAVAILABLE) {
            launcher = ensureCorrectLauncher(launcher, resComponentName);
        }
        Log.i(TAG, "Real launcher: " + Integer.toString(launcher));
        return launcher;
    }

    private static boolean isBootCompleted() {
        return SystemProperties.getInt("sys.boot_completed", 0) == 1;
    }

    public static int getCachedLauncher() {
        return SystemProperties.getInt(KEY_CACHED, -1);
    }

    private static void setCachedLauncher(int launcher) {
        SystemProperties.set(KEY_CACHED, Integer.toString(launcher));
    }

    public static String getLauncherComponentName(Context context) {
        final int cachedlauncher = getCachedLauncher();
        int launcher;
        if (isBootCompleted() && cachedlauncher != -1) {
            launcher = cachedlauncher;
        } else if (cachedlauncher != -1) {
            launcher = cachedlauncher;
        } else {
            launcher = getRealLauncher(context);
            setCachedLauncher(launcher);
        }
        Log.i(TAG, "Set launcher: " + Integer.toString(launcher));
        switch (launcher) {
            case LAUNCHER_PIXEL:
                return PIXEL_LAUNCHER_PKG_NAME + RECENT_COMPONENT_NAME;
            case LAUNCHER_NAMELESS:
                return NAMELESS_LAUNCHER_PKG_NAME + RECENT_COMPONENT_NAME;
            case LAUNCHER_LAWNCHAIR:
                return LAWNCHAIR_PKG_NAME + RECENT_COMPONENT_NAME;
            default:
                return context.getString(
                        com.android.internal.R.string.config_recentsComponentName);
        }
    }
}
